# Lab7 - Rollbacks and reliability

## Homework

### Part 1 (2/3 points)

Used commands

```
CREATE DATABASE lab;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Bob', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

### Part 2 (3/3 points)

Used commands

```
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    UNIQUE(username, product)
);

CREATE OR REPLACE FUNCTION check_inventory_limit_size() RETURNS TRIGGER AS $$
DECLARE
    total_sum_amount INT;
BEGIN
    SELECT SUM(amount) INTO total_sum_amount FROM Inventory WHERE username = NEW.username;
    IF total_sum_amount > 100 THEN
        RAISE EXCEPTION 'Cannot place more than 100 items in user''s inventory';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
END;

CREATE TRIGGER inventory_limit BEFORE INSERT ON inventory FOR EACH ROW EXECUTE FUNCTION check_inventory_limit_size();
```

### Extra part (1.5 points)
I believe that first part was easy for you. It requires to know some industry standards. But what if everything is commited but function still fail(e.g. electricity shortage, client-server connection broken or something else). In this case some clients might try to retry the request.
What would happen then? What is your solution for this scenario?

### Extra part (1.5 points)
Ok, working with single database is pretty simple, what if you have 2 or more storages? It might be not only databases, but external REST CRUD services or message queues.
And it is possible that one of that external storage have no option to rollback or made revers changes.

What is possible approaches here?

As an example you might refer to next:
```python
result = decrease_balance_in_db()
message_queue.send({"task": "start_money_withdrawal", "user_id": user_id, "amount": amount})
```
So in this case, due to long real money withdrawal, this operation would be made in background.
And that might be a case, that amount of money in database is decreased, but real money is not changed.

What are possible approaches here?
Let's imagine that you can not merge that into single operation

P.S.3 If you would do extra tasks, I consider just text answers with examples of code. But it is really hard to find changes if you are changing this file(REAMDE.md). So, please, add new files with solutions.
